require 'spec_helper'

describe Rot13 do
  it 'has a version number' do
    expect(Rot13::VERSION).not_to be nil
  end

  describe Rot13::Cipher do
    it 'can apply the ROT13 cipher to a string' do
      expect(Rot13::Cipher.rot13("hello world")).to eq("uryyb jbeyq")
    end

    it 'can "decrypt" a ROT13-ciphered string' do
      expect(Rot13::Cipher.rot13("uryyb jbeyq")).to eq("hello world")
    end

    it 'ignores anything other than the letters A-Z and a-z' do
      expect(Rot13::Cipher.rot13("hello world!")).to eq("uryyb jbeyq!")
      expect(Rot13::Cipher.rot13("uryyb jbeyq!")).to eq("hello world!")
      expect(Rot13::Cipher.rot13("Phone no.: +1 234 56789")).to eq("Cubar ab.: +1 234 56789")
      expect(Rot13::Cipher.rot13("Cubar ab.: +1 234 56789")).to eq("Phone no.: +1 234 56789")
    end
  end
end

describe String do
  require "rot13/extensions"
  it 'can optionally extend the String class' do
    expect("hello world".rot13).to eq("uryyb jbeyq")
    expect("uryyb jbeyq".rot13).to eq("hello world")
  end
end