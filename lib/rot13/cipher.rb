module Rot13
  # The actual Cipher.
  class Cipher
    # Apply the ROT13 cipher to a string.
    # 
    # @param [String] str the string to cipher
    def self.rot13(str)
      str.tr 'A-Za-z', 'N-ZA-Mn-za-m'
    end
  end
end