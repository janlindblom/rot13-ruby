require "rot13/cipher"

class String
  # Apply the ROT13 cipher to this string.
  def rot13
    Rot13::Cipher.rot13(self)
  end
end