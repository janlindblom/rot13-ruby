require "thor"
require "rot13/version"
require "rot13/cipher"

# Implementation of the ROT13 cipher.
module Rot13
  class Cli < Thor
    desc "encode STRING", "encodes a string"
    def encode(str)
      say Cipher.rot13 str
    end

    desc "decode STRING", "decodes a string"
    def decode(str)
      say Cipher.rot13 str
    end
  end
end
