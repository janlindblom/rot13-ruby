# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rot13/version'

Gem::Specification.new do |spec|
  spec.name          = "rot13-ruby"
  spec.version       = Rot13::VERSION
  spec.authors       = ["Jan Lindblom"]
  spec.email         = ["janlindblom@fastmail.fm"]

  spec.summary       = %q{Simple rot13 cipher implementation. Installs the script "rot13".}
  spec.homepage      = "https://bitbucket.org/janlindblom/rot13-ruby"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.3"
  spec.add_development_dependency "pry", "~> 0.10.3"
  spec.add_development_dependency "yard", "~> 0.8.7.6"
  spec.add_runtime_dependency "thor", "~> 0.19.1"
end
